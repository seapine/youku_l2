/* Youku L2 */

#define BOARD_PID		"YK-L2"
#define BOARD_NAME		"Youku-L2"
#define BOARD_DESC		"Youku L2 Dual Band Wireless Router"
#define BOARD_VENDOR_NAME	"Youku Inc."
#define BOARD_VENDOR_URL	"http://www.youku.com/"
#define BOARD_MODEL_URL		"http://yj.youku.com/"
#define BOARD_BOOT_TIME		30
#define BOARD_FLASH_TIME	120
#define BOARD_GPIO_BTN_RESET	18
#define BOARD_GPIO_BTN_WPS	17
#undef  BOARD_GPIO_LED_ALL
#undef  BOARD_GPIO_LED_WIFI
#undef  BOARD_GPIO_LED_SW2G	14
#undef  BOARD_GPIO_LED_SW5G	15
#define BOARD_GPIO_LED_POWER	13
#undef  BOARD_GPIO_LED_LAN
#define BOARD_GPIO_LED_WAN	16
#define BOARD_GPIO_LED_USB	14
#undef  BOARD_GPIO_LED_ROUTER
#undef  BOARD_GPIO_PWR_USB
#define BOARD_HAS_5G_11AC	1
#define BOARD_NUM_ANT_5G_TX	2
#define BOARD_NUM_ANT_5G_RX	2
#define BOARD_NUM_ANT_2G_TX	2
#define BOARD_NUM_ANT_2G_RX	2
#define BOARD_NUM_ETH_LEDS	0
#define BOARD_HAS_EPHY_L1000	1
#define BOARD_HAS_EPHY_W1000	1
#define BOARD_NUM_UPHY_USB3	1
#define BOARD_USB_PORT_SWAP	0
#define BOARD_NUM_ETH_EPHY	3
